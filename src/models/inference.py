import joblib
import pandas as pd
import numpy as np

from typing import List
from sklearn.preprocessing import OneHotEncoder

from src.models.utils import spit_by_user_agent, check_ip, get_user_agent_info, get_invalid_size, get_invalid_code

one_hot_enc = joblib.load('src/models/weights/one_hot_enc.joblib')
model = joblib.load('src/models/weights/kmeans.joblib')


def prepare_inference_data(data: pd.DataFrame,
                           enc: OneHotEncoder,
                           cat_columns: List[str],
                           num_columns: List[str]) -> np.array:

    """Подготаливаем данные для инференса моделей"""

    # Split CLIENT_USERAGENT
    data['CLIENT_USERAGENT'] = data['CLIENT_USERAGENT'].apply(spit_by_user_agent).fillna('None')

    # Формируем фичу на валидность IP
    data['VALID_IP'] = data['CLIENT_IP'].apply(check_ip).astype('int')

    # Get features from CLIENT_USERAGENT
    browser_family, os_family, device_brand = get_user_agent_info(data)

    # Create new colls
    data['browser_family'.upper()] = browser_family
    data['os_family'.upper()] = os_family
    data['device_brand'.upper()] = device_brand

    # В данном случае я тоже оставляю None как отдельный класс
    data['DEVICE_BRAND'] = data['DEVICE_BRAND'].fillna('Other')

    # SUSPICIOUS_DATA
    suspicious_data = [(data['MATCHED_VARIABLE_SRC'].isna()) &
                       (data['MATCHED_VARIABLE_NAME'].isna()) &
                       (data['MATCHED_VARIABLE_VALUE'].isna()) &
                       (data['EVENT_ID'].isna())][0].astype('int').values
    data['SUSPICIOUS_DATA'] = suspicious_data

    # INVALID_SIZE
    invalid_size = data['REQUEST_SIZE'].apply(get_invalid_size)
    data['INVALID_SIZE'] = invalid_size.astype('int')

    # INVALID_CODE
    invalid_code = data['RESPONSE_CODE'].apply(get_invalid_code)
    data['INVALID_CODE'] = invalid_code.astype('int')

    cat_data = data[cat_columns]
    cat_data = enc.transform(cat_data)
    cat_data = cat_data.toarray()
    cat_features = enc.get_feature_names()

    new_data = pd.DataFrame(data=cat_data, columns=cat_features)
    new_data[num_columns] = data[num_columns].values
    new_data = new_data.astype('float')

    return new_data.values
