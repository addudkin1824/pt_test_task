import json
import traceback

from aiohttp import web

VALID_COLUMNS = ['CLIENT_IP', 'CLIENT_USERAGENT', 'MATCHED_VARIABLE_SRC',
                     'MATCHED_VARIABLE_NAME', 'MATCHED_VARIABLE_VALUE', 'EVENT_ID',
                     'REQUEST_SIZE', 'RESPONSE_CODE']

STATUS_CODES = {
    0:
        {
            "code": "0",
            "title": "Сервис загрузки временно недоступен",
            "text": "Попробуйте ещё раз или проверьте правильность отправленных данныех"
        },
    1:
        {
            "code": "1",
            "title": "Некорректный список колонок в Data Frame",
            "text": f"Валидный список колонок - {VALID_COLUMNS}",
        },
}


def service_response(status_code: int = None,
                     response_obj: dict = None,
                     print_traceback_to_log=True):
    """

    """
    if print_traceback_to_log:
        traceback.print_exc()

    if status_code is not None:
        response_obj["result"]["answers"].append(STATUS_CODES[status_code])

    response_str = json.dumps(response_obj, ensure_ascii=False)
    return web.Response(text=response_str, content_type='application/json')
