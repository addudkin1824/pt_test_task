import ipaddress
from user_agents import parse


def get_invalid_size(row):
    try:
        int(row)
        return False
    except:
        return True


def get_invalid_code(row):
    try:
        if len(str(row))>3:
            return True
        else:
            return False
    except:
        return True


def check_ip(ip):
    try:
        ipaddress.ip_address(ip)
    except ValueError:
        return False
    else:
        return True


def spit_by_user_agent(row):
    # Некоторые User-Agent с приставкой "User-Agent: "
    try:
        if "User-Agent: " in row:
            splited = row.split("User-Agent: ")
            return splited[1]
        else:
            return row
    except:
        return row


def get_user_agent_info(data):
    "Достаем мета инфу с User-Agent"
    browser_family = []
    os_family = []
    device_brand = []

    for agent in data['CLIENT_USERAGENT']:
        user_agent = parse(agent)
        browser_family.append(user_agent.browser.family)
        os_family.append(user_agent.os.family)
        device_brand.append(user_agent.device.brand)

    return browser_family, os_family, device_brand
