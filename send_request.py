import requests
import json

import pandas as pd


url = 'http://0.0.0.0:8080'


def send_request2(sample):
    response = requests.post(
        url,
        files={'data': json.dumps(sample),
               'json': (
                   None, json.dumps({"request_data": {"doc_id": 10, "return_boxes": True}}), 'application/json')},

    )
    response = response.json()

    if "response_data" in response:
        return response["response_data"]

    return response


if __name__ == '__main__':
    path2data = 'notebooks/part_10.csv'
    data = pd.read_csv(path2data)
    # # Для ошибки
    # coll = list(data.columns)
    # coll[0] = 'asdasd'
    # data.columns = coll
    sample = data.iloc[:10, :].to_dict()
    answer = send_request2(sample)
    print(answer)


