import os
import joblib
import pandas as pd
from typing import List
from src.models.inference import prepare_inference_data
from src.models.responce import STATUS_CODES, service_response


os.environ['OMP_THREAD_LIMIT'] = '6'


def get_labels(data: pd.DataFrame) -> List[int]:
    """Предсказывает лейблы на полученных дынных"""
    # Подготавливаем данные для инференса
    data = prepare_inference_data(data, one_hot_enc, CAT_COLUMNS, NUM_COLUMNS)
    # Предсказываем лейблы
    labels = model.predict(data)
    return labels.tolist()


def check_coll(columns):
    for coll in VALID_COLUMNS:
        if coll not in columns:
            return False
    return True


def processing_logic_for_service_app(request_obj: dict,
                                     data_list: List[pd.DataFrame]):
    """

    """
    response_obj = {"result": {"answers": []}}
    try:
        for data in data_list:
            data = pd.DataFrame.from_dict(data)
            if check_coll(data.columns):
                answer = get_labels(data)
                response_obj["result"]["answers"].append(answer)
            else:
                response_obj["result"]["answers"].append(STATUS_CODES[1])
    except Exception as e:
        return service_response(status_code=0, response_obj=response_obj)

    return service_response(response_obj=response_obj, print_traceback_to_log=False)


# Запуск сервиса aiohttp на порту 8080
if __name__ == '__main__':
    from aiohttp_service_builder import build_aiohttp_service_app

    one_hot_enc = joblib.load('src/models/weights/one_hot_enc.joblib')
    model = joblib.load('src/models/weights/kmeans.joblib')

    CAT_COLUMNS = ['BROWSER_FAMILY', 'OS_FAMILY', 'DEVICE_BRAND']
    NUM_COLUMNS = ['VALID_IP', 'SUSPICIOUS_DATA', 'INVALID_SIZE', 'INVALID_CODE']

    VALID_COLUMNS = ['CLIENT_IP', 'CLIENT_USERAGENT', 'MATCHED_VARIABLE_SRC',
                     'MATCHED_VARIABLE_NAME', 'MATCHED_VARIABLE_VALUE', 'EVENT_ID',
                     'REQUEST_SIZE', 'RESPONSE_CODE']

    build_aiohttp_service_app(processing_logic_for_service_app)
