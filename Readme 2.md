# Кластеризация http запросов

-  Рабочие ноутбуки с результатами находятся в notebooks

# Запуск сервиса

## Create env
- conda create --name pt_test_task python=3.8.
- conda activate pt_test_task
- pip install -r requirements.txt

## Запуск из консоли
- python app.py (поднимается сервис на порту 8080)
- python send_request.py (пример запроса на сервис)


## Запуск с Docker

### Docker pull
 
### Docker run
- docker run -t -p 8080:8080 pt_test_task

### Send request
- python send_request.py

# Формат данных в запросе
- {coll1: {0: num1, 1: num2}, coll2: {0: num1, 1: num2}}

## Допущенные ключи:
- CLIENT_IP
- CLIENT_USERAGENT
- MATCHED_VARIABLE_SRC
- MATCHED_VARIABLE_NAME
- MATCHED_VARIABLE_VALUE
- EVENT_ID
- REQUEST_SIZE
- RESPONSE_CODE

## Формат ответа
- {'result': {'answer': [[label1, label2, label3...]]}}
