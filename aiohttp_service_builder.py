import io
import json
import time
import pandas as pd
import asyncio
from io import StringIO
from datetime import datetime
from aiohttp import web, MultipartReader
from src.models.responce import service_response


def build_aiohttp_service_app(main_job_function, port=8080):
    # функция для печати ошибки в лог и формирования json ответа сервиса

    # Основная функция-каркас(пайплайн) для обработки документов
    async def process_request_pipeline(request):
        # Объект который будет сериализован в json и отправлен в ответ на запрос
        request_obj = {}
        # Объект для хранения файлов пришедших в запросе
        files_bytes = []
        # список хранящий пришедшие изображения
        data_list = []

        total_start = time.time()

        print('===================================')
        print(datetime.now().strftime("%d/%m/%Y %H:%M:%S"))

        # Разбираем запрос
        try:
            if request.content_type == 'multipart/form-data':
                reader = MultipartReader.from_response(request)
                while True:
                    part = await reader.next()
                    if part is None:
                        break
                    if 'Content-Type' in part.headers and part.headers['Content-Type'] == 'application/json':
                        request_obj = await part.json()
                        if type(request_obj) == str:
                            request_obj = json.loads(request_obj)
                            print(request_obj)
                    else:
                        # считываем данных в массив
                        data_ = await part.read()
                        files_bytes.append(bytes(data_))
            else:
                return service_response(status_code=0)
        except Exception:
            return service_response(status_code=0)

        try:
            start = time.time()

            for file_bytes in files_bytes:

                s = str(file_bytes, 'utf-8')
                data = json.load(StringIO(s))
                data_list.append(data)

            print("Время считывания данных: {0:.2f}".format(time.time() - start))
        except Exception:
            return service_response(status_code=1, response_obj=request_obj.get("request_data"))

        # ================= Работа встраивоемой в каркас логики сервиса ========================
        response = main_job_function(request_obj, data_list)
        # ================= Работа встраивоемой в каркас логики сервиса ========================

        del data_list, files_bytes

        print(f"Общее время: {time.time() - total_start:.2f}c")

        return response

    async def health(request):
        return web.json_response({"health_status": "running"}, status=200, content_type='application/json')

    # Создаем экземпляр веб сервиса
    # увеличиваем максимльный размер тела входящего запроса
    app = web.Application(
        client_max_size=1024 ** 3,
    )

    # Начинам слушать Post запросы приходящие на корневой адрес и обрабатывать нашей основной функцией
    # порт в дальнейшем разруливает gunicorn, но по умолчанию будет 8080
    app.add_routes([
        web.post('/', process_request_pipeline),
        web.post('/predict/', process_request_pipeline),
        web.post('/predict', process_request_pipeline),
        web.get('/health/', health),
        web.get('/health', health),
        web.get('/', health),
    ])

    # запуск приложения из скрипта
    web.run_app(app, port=port)